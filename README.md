# Field Slideshow

Provides a Slideshow format for displaying Image fields, using the JQuery Cycle
2 plugin.

Compared to Views slideshows, building the slideshow from multiple nodes,
this module builds it from a single node, if you're using a multi-valued
Image field.


## Table of contents

- Requirements
- Installation
- Usage
- Maintainers


## Requirements

- [Libraries API](https://www.drupal.org/project/libraries)
- [Jquery Cycle](https://jquery.malsup.com/cycle/download.html) plugin
- [JCarousel](https://github.com/jsor/jcarousel/archive/0.2.9.zip)


## Installation

1. Use composer require `drupal/field_slideshow:^3.x`
2. Download the JQuery Cycle 2 plugin [here](https://github.com/zakgreene/cycle2/)
   (It is fork from original Cycle2 repo which support the latest jQuery
   version) (don't choose the Lite version), and move the downloaded
   jquery.cycle2.min.js file into /libraries/jquery.cycle2/
3. Optionally download swipe plugin from
   [Jquery cycle2 swipe](https://malsup.github.io/min/jquery.cycle2.swipe.min.js)
4. Install Drupal Colorbox module if you want to use colorbox modal.


## Usage

1. Enable Field Slideshow at /admin/modules.
2. Create or edit a content type at /admin/structure/types and
   include an Image field.
3. Edit this image field, so that multiple image files may be added
   ("Number of values" setting at admin/structure/types/manage/
   {content type}/fields/{field_image}).
4. Go to "Manage display" for your content type
   (/admin/structure/types/manage/{content type}/display) and
   switch the format of your multiple image field from Image to Slideshow.
5. Click the settings wheel in the slideshow-formatted multiple
   image field to edit advanced settings.
6. Save! and here you go.


## Maintainers

- Szczepan Musial - [lamp5](https://www.drupal.org/u/lamp5)
- Ajit Shinde - [AjitS](https://www.drupal.org/u/ajits)
- deepikakaran - [deepikakaran](https://www.drupal.org/u/deepikakaran)
- idflood - [idflood](https://www.drupal.org/u/idflood)
- Jérôme Danthinne - [jdanthinne](https://www.drupal.org/u/jdanthinne)
- Liang Shen - [shenzhuxi](https://www.drupal.org/u/shenzhuxi)
